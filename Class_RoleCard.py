""" 
The Role Card class.  Each instance will represent one of the 8 role cards.

Note:
Tightly coupled with "Class_RoleCards".  So much so, that Class_RoleCard should not 
be accessed from anywhere, other than directly by Class_RoleCards.
"""

class RoleCard():
    """
    An instance represents one of the Role Cards that comprise the full Role Card Deck.
    
    Args:
        name:  Str, the name of the role
    """
    def __init__(self, name):
        self._name = name 
        self._money = 0
        self._owner = 'Supply'

    def takeRole(self, playerNumber):
        """
        Called when a player takes a role card.
        The card's owner is updated to the player number.  Any money on the card is returned.
        """
        self.__moneyBack = self._money
        self._money = 0
        self._owner = playerNumber
        return self.__moneyBack