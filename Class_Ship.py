""" 
The Ship class.  Each instance will represent one ship.
"""

class Ship():
    """
    An instance represents one of the ships that exists in a game of Puerto Rico.
    
    Args:
        numOfSlots:  Int, the number of goods slots the ship has.
    """
    def __init__(self, shipNumber, numOfSlots):
        self._name = 'ship'+str(shipNumber)
        self._numOfSlots = numOfSlots
        self._ownerPlayerNumber = None
        self._status = 'Empty'
        self._goodType = ''
        self._goodsLoaded = 0
        self._slotsLeft = numOfSlots - self._goodsLoaded

    def loadGood(self, good, amount):
        """
        Load the stated good and amount onto a ship.
        Ship status, slotsLoaded and slotsLeft adjusted.
        Checks performed to make sure the good and amount are permissable in the rules of the game.
        """
        if self._goodType != '':
            assert self._goodType == good , 'Incorrect good type {} being loaded onto a ship already holding {}!'.format(good, self._goodType)
        assert amount <= self._slotsLeft, 'Trying to load more goods than a ship has slots to take!'
        # Load the goods:
        self._goodType = good
        self._goodsLoaded += amount
        self._slotsLeft = self._numOfSlots - self._goodsLoaded
        assert self._slotsLeft >= 0, 'Error - Ship has negative slots loaded.'
        if self._slotsLeft == 0:
            self._status = 'Full'
        elif self._numOfSlots > self._slotsLeft > 0:
            self._status = 'Partial'
        self._checkShip()

    def emptyShip(self, supply):
        """
        All goods loaded on the ship returned to the supply, and the shipStatus updated to 'Empty'
        """
        beforeGood, beforeAmount = self._goodType, self._goodsLoaded          # Save for writing to the log file.
        supply.adjustGoodsSupply(self._goodType, self._goodsLoaded)
        self._status = 'Empty'
        self._goodType = ''
        self._goodsLoaded = 0
        self._slotsLeft = self._numOfSlots - self._goodsLoaded
        self._checkShip()
        return self._name, beforeGood, beforeAmount

    def _checkShip(self):
        """
        Runs some edits to make sure the ship is adhering to game rules.
        """        
        assert self._status in ('Full','Partial','Empty'), 'Invalid ship state of {}'.format(self._status)
        if self._status == 'Full':
            assert self._goodsLoaded == self._numOfSlots, 'A ship has status full, but total slots is {} and total goods loaded is {}'.format(self._numOfSlots, self._goodsLoaded)
            assert self._slotsLeft == 0, 'A ship is full but it still has {} slots left to fill!'.format(self._slotsLeft)
            assert self._goodType in ['corn','indigo','sugar','tobacco','coffee'], 'A ship is full, but goods type is set to {}'.format(self._goodType) 
        if self._status == 'Partial':
            assert self._slotsLeft != 0, 'A ship is partially loaded, but slots left is 0'
            assert self._goodsLoaded != 0, 'A ship is partially loaded, but goods loaded is 0' 
            assert self._numOfSlots >= self._goodsLoaded >= 0, 'A ship has {} slots, but is partially loaded with {} goods'.format(self._numOfSlots, self._goodsLoaded)
            assert self._goodType in ['corn','indigo','sugar','tobacco','coffee'], 'A ship is partially loaded, but goods type is set to {}'.format(self._goodType) 
        if self._status == 'Empty':
            assert self._goodsLoaded == 0, 'A ship is partially loaded, but goods loaded is not 0' 
            assert self._goodType == '', 'A ship is empty, but goods type is set to {}'.format(self._goodType) 

    # Getters
    @property
    def slotsLoaded(self):
        return self._goodsLoaded
    @property
    def slotsLeft(self):
        return self._slotsLeft
    @property
    def numOfSlots(self):
        return self._numOfSlots
    @property
    def goodType(self):
        return self._goodType
    @property
    def status(self):
        return self._status

    
