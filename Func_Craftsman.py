"""
The Craftsman Function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog

def Craftsman(f, supply, player):
    """
    Performs the Craftsman role.   
    The player will produce goods, acccording to their Goods Production score.
    The player can never take more goods than the supply has avaliable.

    Args:
        f:           The log file object
        Supply:      The supply of game resources
        player:      The active player object
    """
    playerGoodProduction = player.goodProduction
    goodsSupply = supply.goodsSupply
    productionLog = []
    for goodToProduce in playerGoodProduction:
        if playerGoodProduction[goodToProduce] > 0:
            amountRequested = playerGoodProduction[goodToProduce]
            # If the supply has fewer goods that the amount requested, reduce amount requested
            amountRequested = min(goodsSupply[goodToProduce], amountRequested)
            if amountRequested > 0:
                # Add the good to the player, reduce goodsSupply, add to the log for reporting.
                player.adjustGood(goodToProduce, amountRequested)
                supply.adjustGoodsSupply(goodToProduce, amountRequested*-1)
                productionLog.append([goodToProduce, amountRequested])
    
    Func_WriteLog.logCraftsman(f, player.number, productionLog)


def CraftsmanPriv(f, supply, player):
    """
    Performs the Craftsman's priviledge.   
    After all players have taken their goods, the priviledge is to take one extra good from those left.
    If a more expensive good is avalible, it will be preffered.

    Args:
        f:              The log file object
        Supply:      The supply of game resources
        player:         The active player object
    """
    playerGoodProduction = player.goodProduction
    goodsSupply = supply.goodsSupply
    goodShortlist = []
    # Prepare the shortlist.  If a player can produce a good, and the supply has >0 of it - the good qualifies.
    for goodToProduce in playerGoodProduction:
        if playerGoodProduction[goodToProduce] > 0:
            if goodsSupply[goodToProduce] > 0:
                goodShortlist.append(goodToProduce)
    #Choose the most expensive good from the shortlist:
    if 'coffee' in goodShortlist:
        chosenGood = 'coffee'
    elif 'tobacco' in goodShortlist:
        chosenGood = 'tobacco'
    elif 'sugar' in goodShortlist:
        chosenGood = 'sugar'
    elif 'indigo' in goodShortlist:
        chosenGood = 'indigo'
    elif 'corn' in goodShortlist:
        chosenGood = 'corn'
    else:
        chosenGood = None

    if chosenGood is not None:
        # Add the good to the player, reduce goodsSupply, add to the log for reporting.
        player.adjustGood(chosenGood, 1)
        supply.adjustGoodsSupply(chosenGood, -1)

    Func_WriteLog.logCraftsmanPriv(f, player.number, chosenGood)
    return goodsSupply