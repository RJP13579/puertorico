""" 
The Prospector function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog

def Prospector(f, player):
    """ 
    Prospector Function.  Very simple, it just 
    adds +1 money to the priviledge player.
    
    Args:
        f:       The log file object
        player:  The active player object
        priv:    Boolean, True if the player has the priviledge
    """
    
    player.adjustMoney(1)
    Func_WriteLog.logProspector(f, player.number)
    
