""" 
The Player class.  Each instance will represent one player.
"""

class Player():
    """
    An instance represents one of the players playing the game of Puerto Rico.
    
    Args:
        number: Int, the players number
        name:   Str, the player's name
        money:  Int, the starting money for the player
    """   
    def __init__(self, number, name, money):
        self._number = number
        self._name = name
        self._role = None
        self._quarries = 0    
        self._money = money
        self._goodsBuildingScore = {'corn':10, 'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0}    #Corn defaults to maximum building capacity
        self._goodProduction = {'corn':0, 'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0}
        self._goodBalance = {'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0}                      #Balance is used when deciding what plantation/building to choose.  
        self._ownedGoods = {'corn':0, 'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0}             #Balance <> 0 means either plantations or buildigs for that good are required.
        self._productionBuildings = []
        self._violetBuildings = []
        self._plantations = {'corn':0, 'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0}
        self._goodsVPs = 0
        self._buildingVPs = 0
        self._extraVPs = 0

    def adjustMoney(self, money):
        """
        Will adjust a player's money - either adding or subtracting.
        """
        self._money += money
        assert self._money >= 0, 'Error - player has negative money'
    
    def addBuilding(self, building):
        """
        Will add a building to the player.
        Will add to the player's buildingVPs total.
        If a productionBuilding, the goodsBalance will be updated to reflect the change in Supply/Demand for the relevant good.
        """
        building.owner = self._number
        tier, slots, goodType = building.tier, building.slots, building.goodType
        self._buildingVPs += tier
        if goodType is not None:
            self._productionBuildings.append(building)
            self._goodsBuildingScore[goodType] += slots
            self._goodProduction[goodType] = min(self._goodsBuildingScore[goodType], self._plantations[goodType])
            #Adding a production building reduces balance for the good.  (-ve balance indicates too many buildings, not enough plantations)
            self._goodBalance[goodType] -= slots
        else:
            for existingBuilding in self._violetBuildings:
                assert existingBuilding.name != building.name, 'Same player cannot have two of the same violet building type'
            self._violetBuildings.append(building)

    def addPlantation(self, plantation):
        """
        Will add a plantation or a quarry to the player.
        If a plantation, the goodsBalance will be updated to reflect the change in Supply/Demand for the relevant good.
        """
        if plantation != 'quarry':
            self._plantations[plantation] += 1
            self._goodProduction[plantation] = min(self._goodsBuildingScore[plantation], self._plantations[plantation])        
            #Adding a plantation increases balance for the good.  (+ve balance indicates too many plantations, not enough buildings)
            if plantation != 'corn': self._goodBalance[plantation] += 1
        else:
            self._quarries += 1

    def purgeGoods(self, supply):
        """
        Will return all the player's goods to the supply.
        The player will retain 1 of their most valuable good type.
        """
        # Player gets to keep 1 good - Choose the most expensive.
        goodToSave = ''
        self._purgedGoods = {'corn':0, 'indigo':0, 'sugar':0, 'tobacco': 0, 'coffee': 0} 
        if self._ownedGoods['coffee'] > 0:
            goodToSave = 'coffee'
        elif self._ownedGoods['tobacco'] > 0:
            goodToSave = 'tobacco'
        elif self._ownedGoods['sugar'] > 0:
            goodToSave = 'sugar'
        elif self._ownedGoods['indigo'] > 0:
            goodToSave = 'indigo'
        elif self._ownedGoods['corn'] > 0:
            goodToSave = 'corn'
        # Return ALL goods to the goodsSupply:
        for good in self._ownedGoods:
            supply.adjustGoodsSupply(good, self._ownedGoods[good])
            self._purgedGoods[good] += self._ownedGoods[good]
            self._ownedGoods[good] = 0

        # Claim back the saved good:
        if goodToSave != '':
            supply.adjustGoodsSupply(goodToSave, -1)
            self._purgedGoods[goodToSave] -= 1
            self._ownedGoods[goodToSave] += 1

        for good in self._purgedGoods:
            purgeFlag = True if self._purgedGoods[good] > 0 else False
                                
        return self._purgedGoods, purgeFlag

    def assignRole(self, role, money):
        """
        Will assign a role to a player
        """
        self._role = role
        self._money += money

    def adjustGood(self, good, amount):
        """
        Adjust an amount (+ve or -ve) of a good to the player's owned goods total.
        """
        self._ownedGoods[good] += amount
        assert self._ownedGoods[good] >= 0, 'Player has negative goods for good {}!'.format(self._ownedGoods[good])

    def addGoodsVPs(self, amount):
        """
        Add goods VPs to the player when they ship goods via the Captian.Adjust an amount (+ve or -ve) of a good to the player's owned goods total.
        """
        self._goodsVPs += amount

    # Getters & Setters
    @property
    def number(self):
        return self._number
    @property
    def name(self):
        return self._name
    @property
    def role(self):
        return self._role
    @property
    def quarries(self):
        return self._quarries
    @property
    def money(self):
        return self._money
    @property
    def goodBalance(self):
        return self._goodBalance
    @property
    def goodProduction(self):
        return self._goodProduction
    @property
    def ownedGoods(self):
        return self._ownedGoods
    @property
    def productionBuildings(self):
        return self._productionBuildings
    @property
    def violetBuildings(self):
        return self._violetBuildings
    @property
    def plantations(self):
        return self._plantations
    @property
    def goodsVPs(self):
        return self._goodsVPs
    @property
    def buildingVPs(self):
        return self._buildingVPs
    @property
    def extraVPs(self):
        return self._extraVPs
    @property        
    def totalVPs(self):
        return (self._goodsVPs + self._buildingVPs + self._extraVPs)


