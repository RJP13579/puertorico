""" 
End of round check functions.
"""

import Func_WriteLog

def checkStatus(allPlayers, supply, tradingHouse, allShips):
    """ 
    checkStatus function.
    Will perform checks on total artefacts in the game to make sure we have the correct totals.
    Ensure we arn't eroneously adding / removing things from the game.
    
    1:  Checks that the total number of plantations never alters.  Plantations can be split between:
        plantationSupply, Player Objects, faceUpPlantations, discardedPlantations
    2:  Checks there's only ever 8 quarries in the game.  Quarries can be split between:
        quarrySupply, Player Objects.
    3:  The total number of each goods never exceeds the limit for that particular good type.
        Goods can be split between:   goodsSupply, Player Objects, Ship Objects.

    Args:
        allPlayers:             List, of all player objects
        supply:                 Total resources still in the supply
        tradingHouse:           The Trading House object
        allShips:               List, of all ship objects

    Returns:
        gameOK:      Boolean, True if game state is OK.   Otherwise False.
    """
    # Check that the number of Plantations & Quarries has not deviated from the totalPlantations.
    playerPlantationTotal, playerQuarriesTotal = 0, 0
    for player in allPlayers:
        playerQuarries, playerPlantations = player.quarries, player.plantations
        playerQuarriesTotal += playerQuarries
        for plantation in playerPlantations:
            playerPlantationTotal += playerPlantations[plantation]
    assert supply.totalPlantations == playerPlantationTotal + supply.getTotalRemainingPlantations() + supply.discardedPlantations + supply.getTotalFaceUpPlantations(),\
            'Number of plantations has gone out of sync! {} = {} {} {} {}'.format(supply.totalPlantations, playerPlantationTotal, supply.getTotalRemainingPlantations(), supply.discardedPlantations, supply.getTotalFaceUpPlantations()) 
    assert supply.totalQuarries == playerQuarriesTotal + supply.quarrySupply, 'Number of quarries has gone out of sync!'

    # Scan the ships - make sure they are in order.
    goodsOnShips = [ship.goodType for ship in allShips if ship.goodType != '']
    assert len(goodsOnShips) == len(set(goodsOnShips)), 'Two ships have the same good laoded!'
    for ship in allShips:
        if ship.status == 'Empty':
            assert ship.slotsLoaded == 0, 'Ship is empty, but slotsLoaded = {}'.format(ship.slotsLoaded)
            assert ship.goodType == '', 'Ship is empty, but goodType = {}'.format(ship.goodType)
        if ship.status == 'Partial':
            assert ship.numOfSlots >= ship.slotsLeft >= 0, 'Ship is parially loaded, but slotsLoaded {} not between total slots {} and 0'.format(ship.slotsLoaded, ship.numOfSlots)
            assert ship.goodType != '', "Ship is parially loaded, but goodType = ''"
        assert ship.status != 'Full', 'Ship is Full at end of round - not possible, as should have been cleared down!'

    # Check the total goods shared between the supply and the players is the same as the total of all goods in the game:
    # {'corn':9; 'indigo':11; 'sugar':11; 'tobacco':9; 'coffee': 9}
    allGoods = supply.allGoods
    goodsSupply = supply.goodsSupply
    for good in allGoods:
        playerTotal, shipTotal, tradingHouseTotal = 0, 0, 0
        # Sum up total of that good with the players:
        for player in allPlayers:
            playerTotal += player.ownedGoods[good]
        # Add the goods on ships:
        for ship in allShips:
            shipTotal += ship.slotsLoaded if good == ship.goodType else 0
        # Add the goods in the Trading House:
        for tradingHouseGood in tradingHouse.placedGoods:
            tradingHouseTotal += 1 if tradingHouseGood == good else 0
        # Add the total in the supply:
        supplyTotal = goodsSupply[good]
            
        # Uncomment as a debug aid.  Will print out the location of all the goods as this check is performed.
        #print('Good {}, Players: {}, Ship: {}, Supply: {}, TradHse: {}, Target = {}'.format(good, playerTotal, shipTotal, supplyTotal, tradingHouseTotal, allGoods[good]))

        assert (playerTotal + shipTotal + supplyTotal + tradingHouseTotal) == allGoods[good], 'Number of good {} has gone out of sync!'.format(good)

def checkEndOfGame(f, supply, allPlayers):
    """ 
    checkEndOfGame function.
    Will evaluate player objects and game state to see if the game has ended.
    
    1:  If a player has occuplied 12 building slots, set gameEnd = True
    2:  If colonistSupply hits 0, or negative, set gameEnd = True
    3:  If the vpSupply hits 0, or negative, set gameEnd = True

    Args:
        f:               The log file object
        supply:          The supply of game resoueces
        allPlayers:      List, of all player objects

    Returns:
        gameEnd:         Boolean, True if game end is triggered.   Otherwise False.
    """
    gameEnd = False

    # Has a player placed 12 buildings?
    for player in allPlayers:
        playerBuildingTotal = 0
        playerBuildingTotal += (len(player.productionBuildings) + len(player.violetBuildings))
        assert playerBuildingTotal < 13, 'A player has more than 12 buildings placed!'
        if playerBuildingTotal == 12:
            gameEnd = True
            Func_WriteLog.logGameEndReason(f, player.number, 'placed 12 Buildings')
    
    # If we've hit 0 (or negative) colonists in the colonistSupply:
    if supply.colonistSupply <= 0:
        gameEnd = True
        Func_WriteLog.logGameEndReason(f, None, 'No more colonists')

    # VP Supply hits 0 (or negative)
    if supply.vpSupply <= 0:
        gameEnd = True
        Func_WriteLog.logGameEndReason(f, None, 'VP Supply depleted')

    return gameEnd


def printGoods(allPlayers, supply, tradingHouse, allShips):
    """
    printGoods function
    Use to assist code testing.  Place a call to this function to print out
    the location of all the goods at that stage in the program.
    
    Args:
        allPlayers:             List, of all player objects
        supply:                 Total resources still in the supply
        tradingHouse:           The Trading House object
        allShips:               List, of all ship objects
    """
    
    allGoods = supply.allGoods
    goodsSupply = supply.goodsSupply
    for good in allGoods:
        playerTotal, shipTotal, supplyTotal, tradingHouseTotal = 0, 0, 0, 0
        # Sum up total of that good with the players:
        for player in allPlayers:
            playerTotal += player.ownedGoods[good]
        # Add the goods on ships:
        for ship in allShips:
            shipTotal += ship.slotsLoaded if good == ship.goodType else 0
        # Add the total in the supply:
        supplyTotal += goodsSupply[good]
        # Add the goods in the Trading House:
        for tradingHouseGood in tradingHouse.placedGoods:
            tradingHouseTotal += 1 if tradingHouseGood == good else 0
        print('Good {}, Players: {}, Ship: {}, Supply: {}, TradHse: {}, Total = {} ||| Target = {}'.format(good, playerTotal, shipTotal, supplyTotal, tradingHouseTotal,\
              (playerTotal + shipTotal + supplyTotal + tradingHouseTotal), allGoods[good]))