"""
The Trading House class.  Only one instance, to represent one the Trading House component.
"""

class TradingHouse():
    """
    An instance represents the Trading House side-board.
    """
    def __init__(self):
        self._full = False
        self._freeSlots = 4
        self._goodValue = {'corn':0, 'indigo':1, 'sugar':2, 'tobacco':3, 'coffee':4}
        self._allowedGoods = ['corn', 'indigo', 'sugar', 'tobacco', 'coffee']
        self._placedGoods = []

    def tradeGood(self, good):
        """
        Call this to place a good on the Trading House board.
        It will perform edits - e.g. make sure board is not full.
        And will return the amount of money the good has been traded for
        """
        assert self._full == False, 'Attempted to trade on a full Trading House'
        assert good in self._allowedGoods, 'Cannot trade {0} - as {0} already placed in Trading House'.format(good)
        # Place the good on the Trading House and return the good's value
        self._allowedGoods.remove(good)
        self._placedGoods.append(good)
        self._freeSlots -= 1
        assert self._freeSlots >= 0, 'Error - gone below zero free slots on the Trading House'
        if self._freeSlots == 0:
            self._full = True
        return self._goodValue[good]

    def emptyTradingHouse(self, supply):
        """
        Will empty the Trading house, returning all goods to the supply
        and re-initialise the trading house.
        """
        for good in self._placedGoods:
            supply.adjustGoodsSupply(good, 1)
        self.__init__()

    # Getters
    @property
    def allowedGoods(self):
        return self._allowedGoods if self._full == False else []
    @property
    def placedGoods(self):
        return self._placedGoods
    @property
    def isFull(self):
        return self._full





