""" 
The Settler function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog
import random
from Func_Decorators import checkPriv

@checkPriv
def Settler(f, supply, player, priv):
    """
    Performs the Settler role.   
    Will choose a Quarry or Plantation for player from the faceUpPlantations list
    Quarry Choice:
        1:  If player has priviledge and fewer than 2 quarries, take quarry.
        2:  If player has priviledge and fewer than 4 quarries, and there's 
            only plantations with a +ve balance available, take quarry.
    Plantation choice: 
        1:  Will choose a plantation for a good with +ve balance 
        2:  Will choose a plantation for a good with neutral balance, or corn.
        3:  Will choose a plantation for a good with -ve balance 
        4:  Will choose nothing.
    
    Args:
        f:              The log file object
        Supply:         The supply of game resources
        player:         The active player object
        priv:           Boolean, True if the player has the priviledge
    """
    playerNumber, playerQuarries, goodBalance = player.number, player.quarries, player.goodBalance
    choiceMade, plantationShortlist, chosenPlantation = False, [], None
    quarrySupply = supply.quarrySupply
    faceUpPlantations = supply.faceUpPlantations

    # ~~~~~~~~~~~ ~~~~~~~~~~~ ~~~~~~~~~~~
    # Part 1:   Choose either a Quarry, or a plantation from the faceUpPlantations list

    # 0th Choice:  If possible, and Player has fewer than 2 quarries - choose a quarry.
    if priv == True and playerQuarries < 2 and quarrySupply > 0:
        chosenPlantation = 'quarry'
        choiceMade = True        
    # Check if nothing avaliable (only possible late game)
    if len(faceUpPlantations) == 0:
        # Choose a quarry if possible:
        if priv == True and playerQuarries < 4 and quarrySupply > 0:    
            chosenPlantation = 'quarry'
            choiceMade = True            
        else:
            chosenPlantation = None
            choiceMade = True        
    # First choice:   Build a shortlist of all goods with a -ve balance, take a random choice from this shortlist.
    if choiceMade == False:
        for good in goodBalance:
            if goodBalance[good] < 0 and good in faceUpPlantations:
                plantationShortlist.append(good)
        if len(plantationShortlist) > 0:
            chosenPlantation = random.choice(plantationShortlist)
            choiceMade = True
    # Second choice:   Build a shortlist of all goods with a neutral balance and corn, take a random choice from this shortlist.
    if choiceMade == False:
        for good in goodBalance:
            if goodBalance[good] == 0 and good in faceUpPlantations:
                plantationShortlist.append(good)
        for good in faceUpPlantations:
            if good == 'corn':
                plantationShortlist.append(good)
        if len(plantationShortlist) > 0:     
            chosenPlantation = random.choice(plantationShortlist)
            choiceMade = True
    # Third choice:  Take a 3rd or 4th quarry, if possible. 
    if choiceMade == False and priv == True and playerQuarries < 4 and quarrySupply > 0:
        chosenPlantation = 'quarry'
        choiceMade = True           
    # Fourth choice:   Pick a random one from remainder.  (Should only be goods with a +ve balance)
    if choiceMade == False:
        chosenPlantation = random.choice(faceUpPlantations)
        choiceMade = True

    # Now add chosen plantation to the player:
    if chosenPlantation == None:
        pass
    else:
        player.addPlantation(chosenPlantation)
        if chosenPlantation == 'quarry':
            supply.adjustQuarrySupply(-1)
        else:
            supply.takePlantationFromFaceUps(chosenPlantation)
        
    # Write to the log file:
    if chosenPlantation == None:
        Func_WriteLog.logSettler(f, playerNumber, 'Nothing')
    else:
        Func_WriteLog.logSettler(f, playerNumber, chosenPlantation)
