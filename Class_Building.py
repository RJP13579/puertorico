""" 
The Building class.  Each instance will represent one building tile.
"""

class Building():
    """
    An instance represents one of the violet buildings that exists in a game of Puerto Rico.
    
    Args:
        name:  Str, the name of the building
        tier:  Int, the tier of the building (1-4).  The same as the number of VPs the building grants it's purchaser.
        cost:  Int, the full cost to buy the building.
        desc:  Str, A short description of the buildings function
    """
    def __init__(self, name, tier, slots, cost, desc):
        self._name = name
        self._tier = tier
        self._slots = slots
        self._cost = cost
        self._desc = desc
        self._owner = 'Supply'
    
    def getPrice(self, quarries, priv):
        """
        Calculate a buildings Price, factoring in any discounts

        Args:
            quarries:  Int, the number of quarries the player owns.
            priv:      Boolean, set to True is the player has the priviledge

        Return:
            price:     Int, the adjusted cost for the player to buy the building.
        """
        discount = min(self._tier, quarries)
        discount += 1 if priv == True else 0
        price = max(0, self._cost - discount)
        return price

    # Getters & Setters
    @property
    def name(self):
        return self._name
    @property
    def tier(self):
        return self._tier
    @property
    def slots(self):
        return self._slots
    @property
    def goodType(self):
        return None
    @property
    def owner(self):
        return self._owner
    @owner.setter
    def owner(self, owner):
        self._owner = owner


class ProductionBuilding(Building):
    """
    An instance represents one of the production buildings that exists in a game of Puerto Rico.
    This sub-cladd totally unessecary, as the addition of the 'goofType' field can easily be accounted for in the core Building class
    I am deliberately over-engineering an inheritence, purely as a coding exercise.
    
    Args:
        name:     Str, the name of the building.
        tier:     Int, the tier of the building (1-4).  The same as the number of VPs the building grants it's purchaser.
        cost:     Int, the full cost to buy the building.
        desc:     Str, A short description of the buildings function.
        goodType: Str, the name of the good the building produces.
    """
    def __init__(self, name, tier, slots, cost, desc, goodType):
        Building.__init__(self, name, tier, slots, cost, desc)
        self._goodType = goodType
        self._productionScore = slots

    # Getters
    @property
    def goodType(self):
        return self._goodType

