"""
Class for the supply of all various board game components
"""

from Func_InitBuildings import initialiseProductionBuildings, initialiseVioletBuildings
import random

class Supply:

    def __init__(self, numOfPlayers):
        assert numOfPlayers in [3,4,5], 'Incorrect number of players {}'.format(numOfPlayers)
        self._numOfPlayers = numOfPlayers
        # numOfPlayers dependant supply totals:
        if numOfPlayers == 3:
            self._colonistSupply = 55
            self._vpSupply = 75
        elif numOfPlayers == 4:
            self._colonistSupply = 75
            self._vpSupply = 100
        elif numOfPlayers == 5:
            self._colonistSupply = 95        
            self._vpSupply = 122

        # Crete all the production building objects:
        self._sip1, self._sip2, self._sip3, self._sip4, self._ssm1, self._ssm2, self._ssm3, self._ssm4,\
        self._ip1, self._ip2, self._ip3, self._sm1, self._sm2, self._sm3, self._ts1, self._ts2, self._ts3, self._cr1, self._cr2, self._cr3 = initialiseProductionBuildings()
        # Crete all the violet building objects:
        self._smlmkt1, self._smlmkt2, self._hac1, self._hac2, self._conhut1, self._conhut2, self._smlwhse1, self._smlwhse2,\
        self._off1, self._off2, self._lrgmkt1, self._lrgmkt2, self._lrgwhse1, self._lrgwhse2, self._fact1, self._fact2,\
        self._harb1, self._harb2, self._gldhal, self._res, self._fort, self._custhse, self._ctyhal, \
        self._dummy1, self._dummy2, self._dummy3, self._dummy4, self._dummy5, self._dummyPairings = initialiseVioletBuildings()
        # Create a list all all the building objects
        self._buildingSupply = [self._sip1, self._sip2, self._sip3, self._sip4, self._ssm1, self._ssm2, self._ssm3, self._ssm4, self._ip1, self._ip2, self._ip3,\
                        self._sm1, self._sm2, self._sm3, self._ts1, self._ts2, self._ts3, self._cr1, self._cr2, self._cr3,\
                        self._smlmkt1, self._smlmkt2, self._hac1, self._hac2, self._conhut1, self._conhut2, self._smlwhse1, self._smlwhse2, \
                        self._off1, self._off2, self._lrgmkt1, self._lrgmkt2, self._lrgwhse1, self._lrgwhse2, self._fact1, self._fact2, self._harb1, self._harb2,\
                        self._gldhal, self._res, self._fort, self._custhse, self._ctyhal, self._dummy1, self._dummy2, self._dummy3, self._dummy4, self._dummy5]    # dummyPairings omitted from buildingSupply

        # Initiualise the plantation / quarry attributes:
        self._quarrySupply = 8
        self._totalQuarries = 8
        self._discardedPlantations = 0
        self._plantationSupply = ['corn']*10 + ['indigo']*12 + ['sugar']*11 + ['tobacco']*9 + ['coffee']*8
        self._totalPlantations = len(self._plantationSupply)
        self._faceUpPlantations = []
    
        # Initialise the goods:
        self._allGoods = {'corn':10, 'indigo':11, 'sugar':11, 'tobacco':9, 'coffee': 9}
        self._goodsSupply = self._allGoods.copy()


    def dealPlantations(self):
        """
        Re-deal the face up plantations.  Performed the Settler role and at game start.
            1:  Will remove unpicked plantations from the faceUpPlantations list, and track them in the 'discardedPlantations' integer
            2:  Will deal out new plantations (numOfPLayers+1) from the supply into the faceUpPlantation list.
        """
        #Discard the remaining face up plantations.
        self.adjustDiscardedPlantations(len(self._faceUpPlantations))
        self._faceUpPlantations = []
        # Refresh faceUpPlantations from supply
        for i in range(self._numOfPlayers + 1):
            i = i           # Added to remove annoying warning message about unused variable.
            if self.getTotalRemainingPlantations() > 0:
                _chosenPlantation = self.chooseRandomPlantation()
                self.takePlantationFromSupply(_chosenPlantation)
                self._faceUpPlantations.append(_chosenPlantation)

    def adjustColonistSupply(self):
        self._colonistSupply -= (self._numOfPlayers + 3)

    def adjustVpSupply(self, amount):
        self._vpSupply += (amount)

    def takeBuildingFromSupply(self, building):
        self._buildingSupply.remove(building)

    def takeDummyPairingFromSupply(self, tier4Building):
        del self._dummyPairings[tier4Building]

    def adjustQuarrySupply(self, amount):
        self._quarrySupply += amount      #Amount will always be -ve
        assert self._quarrySupply >= 0, 'Dropped below 0 quarries'
    
    def adjustDiscardedPlantations(self, amount):
        self._discardedPlantations += amount

    def getTotalRemainingPlantations(self):
        return len(self._plantationSupply)

    def chooseRandomPlantation(self):
        return random.choice(self._plantationSupply)

    def takePlantationFromSupply(self, chosenPlantation):
        assert chosenPlantation in self._plantationSupply, 'Attempted to remove {0} from supply, but no {0} in supply'.format(chosenPlantation)
        self._plantationSupply.remove(chosenPlantation)

    def getTotalFaceUpPlantations(self):
        return len(self._faceUpPlantations)

    def takePlantationFromFaceUps(self, chosenPlantation):
        assert chosenPlantation in self._faceUpPlantations, 'Attempted to remove {0} from face up plantations, but no {0} in face up plantations'.format(chosenPlantation)
        self._faceUpPlantations.remove(chosenPlantation)

    def adjustGoodsSupply(self, good, amount):
        self._goodsSupply[good] += amount      # Amount can be +ve or -ve
        assert self._goodsSupply[good] >= 0, 'Good Supply of {} dropped below 0'.format(self._goodsSupply[good])

    # Getters & Setters
    @property
    def numOfPlayers(self):
        return self._numOfPlayers
    @property
    def colonistSupply(self):
        return self._colonistSupply
    @property
    def vpSupply(self):
        return self._vpSupply
    @property
    def buildingSupply(self):
        return self._buildingSupply
    @property
    def dummyPairings(self):
        return self._dummyPairings
    @property
    def quarrySupply(self):
        return self._quarrySupply
    @property
    def totalQuarries(self):
        return self._totalQuarries
    @property
    def discardedPlantations(self):
        return self._discardedPlantations
    @property
    def plantationSupply(self):
        return self._plantationSupply
    @property
    def totalPlantations(self):
        return self._totalPlantations
    @property
    def faceUpPlantations(self):
        return self._faceUpPlantations
    @property
    def allGoods(self):
        return self._allGoods
    @property
    def goodsSupply(self):
        return self._goodsSupply