""" 
The Captain function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog
import copy
from Func_Decorators import checkCapPriv

@checkCapPriv
def Captain(f, supply, player, priv, allShips):
    """
    Performs the Captain role.   
    Will scan the ships and the players, goods, and determine what & how many goods the player can ship:
        A ship can only hold 1 type of goods
        Once a certain good type is on a ship, it cannot be loaded onto another ship
        A ship has a certain capacity that cannot be exceeded
        A  player can onl load goods they possess

    Once the shortlist of viable goods & amounts is generted, it's ordered so the good
    with the most amounts to ship is first.
    Then the shortlist is scanned, in order, and the first good that can be loaded is
    loaded onto a ship.  They player gains the VPs. 

    Args:
        f:          The log file object
        supply:     The supply of game resources
        player:     The active player object
        priv:       Boolean, True if the player has the privililedge
        allShips:   List. of all the ship objects
    
    Returns:
        sucessFlag: Boolean.  False if the player shipped nothing.   Used to manage the while
                    loop calling the captain function.  When all player return False, captain while loop ends.
    """
    # Assess the cargo currently on each ship.   If a certain good is 
    # already loaded on a ship, then it is 'locked' as it good cannot be loaded onto an empty ship.
    allShipsCargo = [[ship.goodType, ship.slotsLeft] for ship in allShips]
    lockedGoods = [ship.goodType for ship in allShips]

    # Get the player's owned goods - and adjust the totals.
    # If the good is already loaded on a ship, adjust the amount so the player cannot load more than the ship has capacity for.
    playerGoods = copy.deepcopy(player.ownedGoods)
    for cargoGoodAndAmount in allShipsCargo:
        for playerGood in playerGoods:
            if playerGood == cargoGoodAndAmount[0] and playerGoods[cargoGoodAndAmount[0]] > 0:
                playerGoods[cargoGoodAndAmount[0]] = min(playerGoods[cargoGoodAndAmount[0]], cargoGoodAndAmount[1])

    # Re-Order the player goods, in descending adjustedAmount order.  
    # The goods will be evaluated, to see if they qualify for loading onto a ship.
    # The first good that qualifies will be loaded.
    orderedPlayerGoods = sorted(playerGoods.items(), key=lambda x:x[1], reverse = True)      # Dict returned as a list of tuples.
    goodChosen, chosenGoodAndAmount, chosenShip = False, [], None
    for goodAndAmount in orderedPlayerGoods:
        # Try to load on a ship that already has that good type:
        if goodChosen == False:
            for ship in allShips:
                if goodAndAmount[0] == ship.goodType and goodAndAmount[1] > 0:
                    chosenShip = ship
                    goodChosen = True
                    chosenGoodAndAmount = list(goodAndAmount) 
                    break
        # Otherwise, load the good onto an empty ship:
        if goodChosen == False and goodAndAmount[0] not in lockedGoods  and goodAndAmount[1] > 0:
            for ship in allShips:
                if ship.status == 'Empty':
                    chosenGoodAndAmount = list(goodAndAmount) 
                    # Adjust amount, so that we can't load more goods than the ship has capacity for:
                    chosenGoodAndAmount[1] = min(chosenGoodAndAmount[1], ship.slotsLeft)
                    chosenShip = ship
                    goodChosen = True
                    break                

    # If a good has been chosen, take if from the player board,
    # Add it to the ship and give the player the VPs, subtract the VPs from the supply, and return None
    if chosenGoodAndAmount != []:
        player.adjustGood(chosenGoodAndAmount[0], (chosenGoodAndAmount[1] * -1))
        if priv == True: player.addGoodsVPs(1)
        player.addGoodsVPs(chosenGoodAndAmount[1])
        supply.adjustVpSupply(chosenGoodAndAmount[1]*-1)
        #vpSupply -= chosenGoodAndAmount[1]
        chosenShip.loadGood(chosenGoodAndAmount[0], chosenGoodAndAmount[1])
        sucessFlag = True
        Func_WriteLog.logCaptain(f, player.number, chosenGoodAndAmount[0], chosenGoodAndAmount[1], priv)
    else:
        sucessFlag = False

    return sucessFlag
        
    