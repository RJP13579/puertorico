""" 
The Role Card Deck class.  One instance, representing the deck of all active role cards.

Note:  Highly coupled with the RoleCard class.   So much so, that the Role Card class's
attributes have been hardcoded into the logic.
"""

from Class_RoleCard import RoleCard
import random

class RoleCardDeck():
    """
    An instance represents the deck of all Role Cards to be used in the game of Puerto Rice.
    
    Args:
        numOfPlayers:  Int, the number of players
    """
    def __init__(self, numOfPlayers):
        self._allRoleCards = []
        self._settler = RoleCard('Settler')
        self._mayor = RoleCard('Mayor')
        self._builder = RoleCard('Builder')
        self._craftsman = RoleCard('Craftsman')
        self._trader = RoleCard('Trader')
        self._captain = RoleCard('Captain')
        self._allRoleCards = [self._settler, self._mayor, self._builder, self._craftsman, self._trader, self._captain]
        if numOfPlayers > 3:
            self._prospector1 = RoleCard('Prospector')
            self._allRoleCards.append(self._prospector1)
        if numOfPlayers > 4:
            self._prospector2 = RoleCard('Prospector')
            self._allRoleCards.append(self._prospector2)

    def endOfRound(self):
        """
        Will add +1 money to any RoleCards that were unused in the previous round.
        The the cards that were used, will update the owner to 'Supply' so they're ready to be chosen for the next round
        """
        for roleCard in self._allRoleCards:
            if roleCard._owner == 'Supply':
                roleCard._money += 1
            else:
                roleCard._owner = 'Supply'

    def chooseRole(self, player):
        """
        The player will choose a role.
            First choice:  Will chose randomly from any roles with 1 money.
            Second choice: Will chose randomly from the roles with 0 money.
        """
        chosenRole = None
        self._moneyShortlist, self._noMoneyShortlist = [], []
        for roleCard in self._allRoleCards:
            if roleCard._owner == 'Supply':
                if roleCard._money > 0:
                    self._moneyShortlist.append(roleCard)
                else:
                    self._noMoneyShortlist.append(roleCard)
        assert len(self._moneyShortlist) + len(self._noMoneyShortlist) > 0, 'No Role cards avaliable to choose from!'
        if len(self._moneyShortlist) > 0:
            chosenRole = random.choice(self._moneyShortlist)
        elif len(self._noMoneyShortlist) > 0:
            chosenRole = random.choice(self._noMoneyShortlist)
        # Update role and player:
        money = chosenRole.takeRole(player.number)
        player.assignRole(chosenRole._name, money)

    
        

    
