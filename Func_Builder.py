""" 
The Builder function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog
import random
from Func_Decorators import checkPriv

@checkPriv
def Builder(f, supply, player, priv):
    """
    Performs the Builder role.   
    Will generate a list of viable buildings for the active player to buy, then buy one.
    Computing building cost factors in the player's quarry and priviledge.
    Viable buildings:  
        Is affordable, and only 1 of each violet building types is allowed, is not a 
        dummy building and player cannot exceed 12 occupied building slots.
    Building choice: 
        1:  Will choose a production building if a certain goods production is +ve
        2:  Will choose a violet building
            Note:  If multiple choices, will select the most expensive & affordable option.
        3:  Buy nothing
    
    Args:
        f:              The log file object
        Supply:         The supply of game resources
        player:         The active player object
        priv:           Boolean, True if the player has the priviledge
    """
    playerNumber, quarries, goodBalance, money, playersProductionBuildings, playersVioletBuildings = \
        player.number, player.quarries, player.goodBalance, player.money, player.productionBuildings, player.violetBuildings
    buildingSupply, dummyPairings = supply.buildingSupply, supply.dummyPairings
    initialBuildingOptions, deleteBuildingOptions = set(), set()
    buildingOptions, buildingShortList = [], []
    chosenBuilding = None
    shortlistMade = False
    choiceMade = False

    # ~~~~~~~~~~~ ~~~~~~~~~~~ ~~~~~~~~~~~
    # Part 1:   Build the list of viable buildings.

    # Build an initial set of ALL affordable buildings:
    for building in buildingSupply:
        price = building.getPrice(quarries, priv)
        if price <= money: initialBuildingOptions.add(building)
    # Add dummy buildings to the delete set (dummy buildings only exist to ensure Tier4 buildings take up 2 spaces):
    for building in initialBuildingOptions:
        if building.tier == 0: deleteBuildingOptions.add(building)
    # If player already has a violet building of the same name, add that building to the delete set.
    # Resolve building objects to the building name, not the object name.
    ownedBuildingNames = [building.name for building in playersVioletBuildings]
    for building in initialBuildingOptions:
        if building.name in ownedBuildingNames: deleteBuildingOptions.add(building)
    # If 11 building slots are taken, strip out tier 4 buildings - as they are now too big to fit.
    # No need to check for 12 or more slots are filled - this is checked as part of round tidy up.
    if len(playersProductionBuildings + playersVioletBuildings) == 11:
        for building in initialBuildingOptions:
            if building.tier == 4: deleteBuildingOptions.add(building)

    # Establish the final buildingOptions list - any buildings in initialBuildingOptions but not in deleteBuildingOptions
    buildingOptions = list(initialBuildingOptions.difference(deleteBuildingOptions))


    # ~~~~~~~~~~~ ~~~~~~~~~~~ ~~~~~~~~~~~
    # Part 2:  Choose a building from the buildingOptions list.

    # 0:  If no building options, do nothing
    if len(buildingOptions) == 0:
        chosenBuilding = None
        choiceMade = True
    # 1:  Find goods with +ve balance.  Shortlist made from all affordable buildings that will bring goodBalance to 0 or negative.
    if choiceMade == False and shortlistMade == False:
        requestedGoods = [good for good in goodBalance if goodBalance[good] > 0]
        for good in requestedGoods:
            if good == 'indigo':
                for building in buildingOptions:
                    if building.goodType == 'indigo': buildingShortList.append(building)
            if good == 'sugar':
                for building in buildingOptions:
                    if building.goodType == 'sugar': buildingShortList.append(building)
            if good == 'tobacco':
                for building in buildingOptions:
                    if building.goodType == 'tobacco': buildingShortList.append(building)
            if good == 'coffee':
                for building in buildingOptions:
                    if building.goodType == 'coffee': buildingShortList.append(building)
        if len(buildingShortList) > 0: shortlistMade = True
    # 2:  Build shortlist from all affordable violet buildings.  Violet buildings have goodType = None
    if choiceMade == False and shortlistMade == False:
        buildingShortList = [building for building in buildingOptions if building.goodType == None]
        if len(buildingShortList) > 0: shortlistMade = True
    # ~~~~~~~~    
    # Resolve shortlist - choose at random from the most expensive building(s) in the shortlist.
    # ~~~~~~~~    
    if choiceMade == False and shortlistMade == True:
        # Find the most expensive building cost:
        maxPrice = 0
        for building in buildingShortList:
            if building.getPrice(quarries, priv) > maxPrice: maxPrice = building.getPrice(quarries, priv)
        # Build a final shortlist of all buildimgs that have the most expensive cost.
        finalShortlist = [building for building in buildingShortList if building.getPrice(quarries, priv) == maxPrice]
        # Choose a building at random from the final shortlist.
        chosenBuilding = random.choice(finalShortlist)
        choiceMade = True
    # 3:  If nothing was chosen (e.g. all remaining buildings too expensive) then buy nothing:
    if choiceMade == False:
        chosenBuilding = None
        choiceMade = True       

    # ~~~~~~~~~~~ ~~~~~~~~~~~ ~~~~~~~~~~~
    # Part 3:  Apply chosen building, and report to logFile.

    # If building has been chosen:
    if chosenBuilding is not None:
        supply.takeBuildingFromSupply(chosenBuilding)
        player.adjustMoney(chosenBuilding.getPrice(quarries, priv)*-1)
        player.addBuilding(chosenBuilding)
        # If a tier 4 building, add a dummy building to account for the extra size:
        if chosenBuilding.tier == 4:
            for tier4Building in dummyPairings:
                if tier4Building == chosenBuilding:
                    player.addBuilding(dummyPairings[tier4Building])
                    break
            supply.takeDummyPairingFromSupply(tier4Building)
    
    # Write to the log file:
    if chosenBuilding == None:
        costToPayer = 0
        buildingName = 'Nothing'
    else:
        costToPayer = chosenBuilding.getPrice(quarries, priv)
        buildingName = chosenBuilding.name
    Func_WriteLog.logBuilder(f, playerNumber, buildingName, costToPayer)
