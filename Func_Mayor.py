""" 
The Mayor function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog

def Mayor(f, supply, player):
    """
    Performs the Mayor role.   
    Colonists have been de-scoped, and thus the Mayor function has been reduced 
    to mimic the prospector.  The priviledge player gets +1 Money.
    
    It does one extra thing, it reduces colonistSupply by numOfPlayers + 3.
    This will approximate the count down to the 'No more colonists' game end condition
    
    Args:
        f:              The log file object
        player:         The active player object
        priv:           Boolean, True if the player has the priviledge
        colonistSupply: The number of colonists left in the colonistSupply
        numOfPlayers:   numOfPlayers
    
    Returns:
        colonistSupply: The reduced number of colonists left in the supply.
    """

    # Mayor only called for the active player.
    player.adjustMoney(1)
    Func_WriteLog.logMayor(f, player.number)
    supply.adjustColonistSupply()