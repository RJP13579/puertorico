"""
Function to evaluate the priviledge.
Used as a decorator to the role functions
"""

def checkPriv(roleF):
    """Used to evaluate the Priv flag for the Settler, Builder and Trader Roles"""
    def wrap(f, supply, player, activePlayerNum, *args):
        priv = True if player.number == activePlayerNum else False
        return roleF(f, supply, player, priv, *args)
    return wrap


def checkCapPriv(roleF):
    """Used to evaluate the Priv flag for the Captain role"""
    def wrap(f, supply, player, activePlayerNum, roundOne, *args):
        priv = True if player.number == activePlayerNum and roundOne else False
        return roleF(f, supply, player, priv, *args)
    return wrap
