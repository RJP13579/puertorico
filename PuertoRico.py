"""
   The master program for the Puerto Rico python game.

   -------------------------------------
   v2:  Rod Pell.     01-Feb-2021
        Colonist concept completely de-scoped, Mayor role massively simplified.
        Hospice, University and Warf buildings de-scoped.
        Violet buildings have no game effect - other than to be purchased,
            contribute VPs and occupy the player's board.
"""
from Class_Supply import Supply
from Class_RoleCardDeck import RoleCardDeck
from Class_Player import Player
from Class_Ship import Ship
from Class_TradingHouse import TradingHouse
from Func_EndOfRound import checkStatus, checkEndOfGame, printGoods
from Func_Settler import Settler
from Func_Mayor import Mayor
from Func_Builder import Builder
from Func_Trader import Trader
from Func_Captain import Captain
from Func_Craftsman import Craftsman, CraftsmanPriv
from Func_Prospector import Prospector

import Func_WriteLog
import random
import sys
import os.path
import json


def PuertoRico(f, numOfPlayers, playerNames):
    allPlayers, supply, tradingHouse, roleCardDeck, allShips = InitialiseGame(f, numOfPlayers, playerNames)
    GameRoundLoop(f, numOfPlayers, allPlayers, supply, tradingHouse, roleCardDeck, allShips)
    EndOfGame(f, numOfPlayers, allPlayers)


def InitialiseGame(f, numOfPlayers, playerNames):
    # Create the Players:
    x = ['p1','p2','p3','p4','p5']
    allPlayers = []
    for i in range(numOfPlayers):
        x[i] = Player(i+1, playerNames[i], numOfPlayers-1)      # Starting money = numOfPlayers -1
        allPlayers.append(x[i])
    del x

    # Write new game details to the log
    playerDetails = [[player.number, player.name] for player in allPlayers]
    Func_WriteLog.logNewGame(f, numOfPlayers, playerDetails)
    del playerDetails
    
    # Initialise the supply, tradingHouse & rolecard desk
    supply = Supply(numOfPlayers)
    tradingHouse = TradingHouse()
    roleCardDeck = RoleCardDeck(numOfPlayers)
    
    # Initialise the 3 ships for the game:
    x = ['ship1','ship2','ship3']
    allShips = []
    for i in range(3):
        x[i] = Ship(i+1, i+numOfPlayers+1)
        allShips.append(x[i])
    del x
    
    # Deal the starting plantations to the players & deal the face up plantations for round 1.
    startPlantations = ['indigo','indigo','corn','corn'] if numOfPlayers in (3,4) else ['indigo','indigo','indigo','corn','corn']
    for player in allPlayers:
        supply.takePlantationFromSupply(startPlantations[allPlayers.index(player)])
        player.addPlantation(startPlantations[allPlayers.index(player)])
    supply.dealPlantations()
    del startPlantations

    return allPlayers, supply, tradingHouse, roleCardDeck, allShips
    

def GameRoundLoop(f, numOfPlayers, allPlayers, supply, tradingHouse, roleCardDeck, allShips):
    gameEnd = False
    roundNumber = 1
    governorIndex = -1     # First round adds 1 to GovIndex
    
    while not gameEnd:
        Func_WriteLog.logNewRound(f, roundNumber)
        print('~~~~~~~~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~~~~')
        print('    Round {} '.format(roundNumber))
    
        # New round - advance the Governor Index & Re-organise the player order:
        governorIndex += 1
        governorIndex = 0 if governorIndex == numOfPlayers else governorIndex
        playerOrder = allPlayers[governorIndex:] + allPlayers[:governorIndex]
        # Choose a role in order:
        for player in playerOrder:
            roleCardDeck.chooseRole(player)
        
        # The roles get played out in order:
        for player in playerOrder:
            activeRole = player.role
            privPlayerIndex = player.number - 1
            rolePlayerOrder = allPlayers[privPlayerIndex:]  + allPlayers[:privPlayerIndex]
    
            # ~~~~~~~~~~~~~~~~~~~~~~~~
            # Perform Roles:
            # ~~~~~~~~~~~~~~~~~~~~~~~~
     
            print('\n*****   Doing {} stuff:::\n'.format(activeRole))
            Func_WriteLog.logRoleStart(f, activeRole)
    
            # Basic roles.  Only called for active player, Priv irrelevant (as will always be True).
            if activeRole == 'Mayor':
                Mayor(f, supply, rolePlayerOrder[0])
    
            if activeRole == 'Prospector':
                Prospector(f, rolePlayerOrder[0])
    
            # Normal Roles.  Loop through all players, set Priv = True for active player (via checkPriv decorator).
            if activeRole == 'Builder':
                for player in rolePlayerOrder:
                    Builder(f, supply, player, rolePlayerOrder[0].number)
    
            if activeRole == 'Settler':
                for player in rolePlayerOrder:
                    Settler(f, supply, player, rolePlayerOrder[0].number)
                #Re-Deal the face up plantation tiles.
                supply.dealPlantations()
    
            if activeRole == 'Trader':
                for player in rolePlayerOrder:
                    Trader(f, supply, player, rolePlayerOrder[0].number, tradingHouse)
                # Trader Tidy up:
                if tradingHouse.isFull:
                    tradingHouse.emptyTradingHouse(supply)
                    Func_WriteLog.logTradingHouseClear(f)
        
            if activeRole == 'Craftsman':
                for player in rolePlayerOrder:           # Craftsman does not use @checkPriv decorator
                    Craftsman(f, supply, player)
                # Craftsman Priviledge is it's own special function, performed after Craftsman finishes.
                CraftsmanPriv(f, supply, rolePlayerOrder[0])
        
            # Complex roles.  Loop through players multiple times, until no one is able to perform the role anymore.
            if activeRole == 'Captain':
                activeCaptains = rolePlayerOrder.copy()
                roundOne = True
                while len(activeCaptains) > 0:
                    for player in rolePlayerOrder:
                        if player in activeCaptains:
                            sucessFlag = Captain(f, supply, player, rolePlayerOrder[0].number, roundOne, allShips)
                            if not sucessFlag: activeCaptains.remove(player)
                    roundOne = False
                # Captain Tidy up - empty out full ships.
                for ship in allShips:
                    if ship.status == 'Full':
                        shipName, goods, amount = ship.emptyShip(supply)
                        Func_WriteLog.logEmptyShip(f, shipName, goods, amount)
                # Captain Tidy up - players purge un-shipped goods
                for player in rolePlayerOrder:
                    purgedGoods, purgeFlag = player.purgeGoods(supply)
                    if purgeFlag: Func_WriteLog.logPurgedGoods(f, player.number, purgedGoods)
                        
            Func_WriteLog.logRoleEnd(f, activeRole)
        
     
        # Round end - check everything in order (eg not exceeding supply anywhere)
        checkStatus(allPlayers, supply, tradingHouse, allShips)
    
        # Check for GameEnd.
        gameEnd = checkEndOfGame(f, supply, allPlayers)
    
        # Prepare the role cards & increment the round number:
        roleCardDeck.endOfRound()   
        roundNumber += 1

def EndOfGame(f, numOfPlayers, allPlayers):
    finalStandings = [[player.number, player.name, player.totalVPs] for player in allPlayers]
    # Order via descending VPs:
    sortedFinalStandings = sorted(finalStandings, key=lambda x:x[2], reverse = True) 
    
    # Print the final scores to the log:
    Func_WriteLog.logFinalStandings(f, sortedFinalStandings, numOfPlayers)

    # Print to the terminal:
    print('First Place:  {} (Player{}), with {} VPs!!!!'.format(sortedFinalStandings[0][1], sortedFinalStandings[0][0], sortedFinalStandings[0][2]))
    print('2nd Place:    {} (Player{}), with {} VPs'.format(sortedFinalStandings[1][1], sortedFinalStandings[1][0], sortedFinalStandings[1][2]))
    print('3rd Place:    {} (Player{}), with {} VPs'.format(sortedFinalStandings[2][1], sortedFinalStandings[2][0], sortedFinalStandings[2][2]))
    if numOfPlayers > 3:
        print('4th Place:    {} (Player{}), with {} VPs'.format(sortedFinalStandings[3][1], sortedFinalStandings[3][0], sortedFinalStandings[3][2]))
    if numOfPlayers > 4:
        print('5th Place:    {} (Player{}), with {} VPs'.format(sortedFinalStandings[4][1], sortedFinalStandings[4][0], sortedFinalStandings[4][2]))


if __name__ == '__main__':
    # Open an empty LogFile.txt
    f = open(os.path.join('FlatFiles','log_file.txt'), mode = 'w', encoding = 'utf-8')

    if sys.argv[1:] == []:
        # If no keywords entered on command line, or running the script directly ... 
        # Then do NOT use the GameSetUp.txt and prompt the user to input set up data.
        # Play PuertoRico just once.

        # Enter number of Players:
        numOfPlayers = int(input('Enter the number of players: '))
        if numOfPlayers not in [3,4,5]:
            raise ValueError('Game must have 3, 4, or 5 players. {} is an invalid number of players'.format(numOfPlayers))
        
        # Build a list of the player names:
        playerNames = []
        for i in range(numOfPlayers):
            name = input('Enter name of player {}: '.format(i+1))
            playerNames.append(name)

        # Just run the Puerto Rico program once, then close LogFile.txt
        PuertoRico(f, numOfPlayers, playerNames)
        f.close()

    else:
        # Read numOfPlayers and playerNames from GameSetUp.json 
        # loop through as many PuertoRico games as are nessecary
        with open(os.path.join('FlatFiles','GameSetUp.json'), mode = 'r', encoding = 'utf-8') as setUpFile:
            for line in setUpFile:
                gameSetup = json.loads(line)
                PuertoRico(f, gameSetup['numOfPlayers'], gameSetup['playerNames'])
        f.close()