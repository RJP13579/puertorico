"""
Function to initially instantiate all the building objects
"""

from Class_Building import Building, ProductionBuilding

def initialiseProductionBuildings():
    """
    Instantiate all the production building objets, part of preparing a game of Puerto Rico.   
    
    Returns:
        The following 20 objects:
            sip1, sip2, sip3, sip4, ssm1, ssm2, ssm3, ssm4, ip1, ip2, ip3, sm1, sm2, sm3, ts1, ts2, ts3, cr1, cr2, cr3
        Each one represents a production building object.
    """
    # Create 4x small indigo plant
    sip1 = ProductionBuilding('Small Indigo Plant',1,1,1,'','indigo')
    sip2 = ProductionBuilding('Small Indigo Plant',1,1,1,'','indigo')
    sip3 = ProductionBuilding('Small Indigo Plant',1,1,1,'','indigo')
    sip4 = ProductionBuilding('Small Indigo Plant',1,1,1,'','indigo')
    
    # Create 4x small sugar mill
    ssm1 = ProductionBuilding('Small Sugar Mill',1,1,2,'','sugar')
    ssm2 = ProductionBuilding('Small Sugar Mill',1,1,2,'','sugar')
    ssm3 = ProductionBuilding('Small Sugar Mill',1,1,2,'','sugar')
    ssm4 = ProductionBuilding('Small Sugar Mill',1,1,2,'','sugar')
    
    # Create 3x indigo plant
    ip1 = ProductionBuilding('Indigo Plant',2,3,3,'','indigo')
    ip2 = ProductionBuilding('Indigo Plant',2,3,3,'','indigo')
    ip3 = ProductionBuilding('Indigo Plant',2,3,3,'','indigo')
    
    # Create 3x sugar mill
    sm1 = ProductionBuilding('Sugar Mill',2,3,4,'','sugar')
    sm2 = ProductionBuilding('Sugar Mill',2,3,4,'','sugar')
    sm3 = ProductionBuilding('Sugar Mill',2,3,4,'','sugar')
    
    # Create 3x tobacco storage
    ts1 = ProductionBuilding('Tobacco Storage',3,3,5,'','tobacco')
    ts2 = ProductionBuilding('Tobacco Storage',3,3,5,'','tobacco')
    ts3 = ProductionBuilding('Tobacco Storage',3,3,5,'','tobacco')
    
    # Create 3x coffee roaster
    cr1 = ProductionBuilding('Coffee Roaster',3,2,6,'','coffee')
    cr2 = ProductionBuilding('Coffee Roaster',3,2,6,'','coffee')
    cr3 = ProductionBuilding('Coffee Roaster',3,2,6,'','coffee')

    return (sip1, sip2, sip3, sip4, ssm1, ssm2, ssm3, ssm4, ip1, ip2, ip3, sm1, sm2, sm3, ts1, ts2, ts3, cr1, cr2, cr3)

#  name, tier, slots, cost

def initialiseVioletBuildings():
    """
    Instantiate all the violet building objets, part of preparing a game of Puerto Rico.   
    
    Returns:
        The following 29 objects:
            smlmkt1, smlmkt2, hac1, hac2, conhut1, conhut2, smlwhse1, smlwhse2, off1, off2, lrgmkt1, lrgmkt2, lrgwhse1, lrgwhse2, fact1, fact2, harb1, harb2,
            gldhal, res, fort, custhse, ctyhal, dummy1, dummy2, dummy3, dummy4, dummy5, dummyPairings
        All (except the last one) represent a production building object.
        dummyPairings is not a building object, but a dictionary pairing each Tier4 building object with a dummy building object.
    """
    # Create 2x small market
    smlmkt1 = Building('Small Market',1,1,1,'')
    smlmkt2 = Building('Small Market',1,1,1,'')

    # Create 2x hacienda
    hac1 = Building('Hacienda',1,1,2,'')
    hac2 = Building('Hacienda',1,1,2,'')

    # Create 2x construction hut
    conhut1 = Building('Construction Hut',1,1,2,'')
    conhut2 = Building('Construction Hut',1,1,2,'')

    # Create 2x small warehouse
    smlwhse1 = Building('Small Warehouse',1,1,3,'')
    smlwhse2 = Building('Small Warehouse',1,1,3,'')

    # Create 2x hospice 
    # Hospice out of scope - as only affects colonists.  Colonists out of scope

    # Create 2x office
    off1 = Building('Office',2,1,5,'')
    off2 = Building('Office',2,1,5,'')

    # Create 2x large market
    lrgmkt1 = Building('Large Market',2,1,5,'')
    lrgmkt2 = Building('Large Market',2,1,5,'')

    # Create 2x large warehouse
    lrgwhse1 = Building('Large Warehouse',2,1,6,'')
    lrgwhse2 = Building('Large Warehouse',2,1,6,'')

    # Create 2x factory
    fact1 = Building('Factory',3,1,7,'')
    fact2 = Building('Factory',3,1,7,'')

    # Create 2x university
    # University out of scope - as only affects colonists.  Colonists out of scope

    # Create 2x harbour
    harb1 = Building('Harbour',3,1,8,'')
    harb2 = Building('Harbour',3,1,8,'')

    # Create 2x warf
    # Warf out of scope - as really complicated.

    # Create 1x guild hall
    gldhal = Building('Guild Hall',4,1,10,'')

    # Create 1x residence
    res = Building('Residence',4,1,10,'')

    # Create 1x fortress
    fort = Building('Fortress',4,1,10,'')

    # Create 1x customs house
    custhse = Building('Customs House',4,1,10,'')

    # Create 1x city hall
    ctyhal = Building('City Hall',4,1,10,'')

    # Create 5x dummy / placeholder buildings. 
    #   Allocated alongside a tier 4 building, to ensure a Tier4 building occupies 2 building slots on a player board.
    dummy1 = Building('Dummy1',0,0,0,'')
    dummy2 = Building('Dummy2',0,0,0,'')
    dummy3 = Building('Dummy3',0,0,0,'')
    dummy4 = Building('Dummy4',0,0,0,'')
    dummy5 = Building('Dummy5',0,0,0,'')

    # Pair the Tier4 buildings with a dummy building 
    dummyPairings = {gldhal:dummy1, res:dummy2, fort:dummy3, custhse:dummy4, ctyhal:dummy5}

    return (smlmkt1, smlmkt2, hac1, hac2, conhut1, conhut2, smlwhse1, smlwhse2, off1, off2, lrgmkt1, lrgmkt2, lrgwhse1, lrgwhse2, fact1, fact2, harb1, harb2,\
            gldhal, res, fort, custhse, ctyhal, dummy1, dummy2, dummy3, dummy4, dummy5, dummyPairings)