""" Write the log file for a game of Puerto Rico"""

def logNewGame(f, numOfPlayers, PlayerDetails):
    f.write("\n- - - - - - - - - - - - - - - - - - - - - - - - - -\nA new {} player game of Puerto Rico has started!\n\n".format(numOfPlayers))
    for i in PlayerDetails:
        f.write("    {} is Player{}\n".format(i[1],i[0]))
    f.write("\nPlease wish everyone good luck!\n- - - - - - - - - - - - - - - - - - - - - - - - - -\n")
    
def logNewRound(f, roundNumber):
    f.write('\n   ##############\n   ##############  Round {} begins:\n   ##############'.format(roundNumber)) 

def logRoleStart(f, roleName):
    f.write("\n --------------------\n    The {} is being played:\n\n".format(roleName))

def logRoleEnd(f, roleName):
    f.write("\n    The {} has finished\n --------------------".format(roleName))

def logBuilder(f, playerNumber, chosenBuilding, cost):
    f.write("Player{} has just built a {} for a cost of {}\n".format(playerNumber, chosenBuilding, cost))

def logSettler(f, playerNumber, chosenPlantation):
    f.write("Player{} has just placed a {}\n".format(playerNumber, chosenPlantation))

def logCraftsman(f, playerNumber, productionLog):
    f.write("Player{} has just produced: {}\n".format(playerNumber, productionLog))

def logCraftsmanPriv(f, playerNumber, good):
    f.write("As the priviledge, Player{} has produced 1 extra {}\n".format(playerNumber, good))

def logMayor(f, playerNumber):
    f.write("Player{} has just gained 1 money\n".format(playerNumber))

def logTrader(f, playerNumber, good, amount, priv):
    if priv == True:
        f.write("Player{} has just traded {} for {} money (Inc +1 for priv)\n".format(playerNumber, good, amount))
    else:
        f.write("Player{} has just traded {} for {} money\n".format(playerNumber, good, amount))

def logTradingHouseClear(f):
    f.write("The Trading House has been cleared")

def logCaptain(f, playerNumber, good, amount, priv):
    if priv == True:
        f.write("Player{} has just shipped {} for {} VPs.  (+1 VP for Priv)\n".format(playerNumber, good, amount))
    else:
        f.write("Player{} has just shipped {} for {} VPs.\n".format(playerNumber, good, amount))

def logEmptyShip(f, shipName, goods, amount):
    f.write("{} has been cleared down.  {} {} returned to supply.\n".format(shipName, amount, goods))

def logPurgedGoods(f, playerNumber, purgedGoods):
    f.write("Player {} has had to purge these goods: {}\n".format(playerNumber, purgedGoods))

def logProspector(f, playerNumber):
    f.write("Player{} has just gained 1 money\n".format(playerNumber))

def logGameEndReason(f, playerNumber, reason):
    if playerNumber != None:
        f.write("\n\n*********************\n     Game has ended!   Player {} has {}\n*********************\n\n".format(playerNumber, reason))
    else:
        f.write("\n\n*********************\n     Game has ended!   {}\n*********************\n\n".format(reason))

def logFinalStandings(f, sortedFinalStandings, numOfPlayers):
    f.write('First Place:  {} (Player{}), with {} VPs!!!!\n'.format(sortedFinalStandings[0][1], sortedFinalStandings[0][0], sortedFinalStandings[0][2]))
    f.write('2nd Place:    {} (Player{}), with {} VPs\n'.format(sortedFinalStandings[1][1], sortedFinalStandings[1][0], sortedFinalStandings[1][2]))
    f.write('3rd Place:    {} (Player{}), with {} VPs\n'.format(sortedFinalStandings[2][1], sortedFinalStandings[2][0], sortedFinalStandings[2][2]))
    if numOfPlayers > 3:
        f.write('4th Place:    {} (Player{}), with {} VPs\n'.format(sortedFinalStandings[3][1], sortedFinalStandings[3][0], sortedFinalStandings[3][2]))
    if numOfPlayers > 4:
        f.write('5th Place:    {} (Player{}), with {} VPs\n'.format(sortedFinalStandings[4][1], sortedFinalStandings[4][0], sortedFinalStandings[4][2]))
