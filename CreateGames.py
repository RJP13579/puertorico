"""
Will create a json file of game initiation info to automate the Puerto Rico script
"""

import random
import os.path
import sys
import json

def CreateGames():
    if sys.argv[1:] == []:
        numOfGames = int(input('How many games do you want to run: '))
        numOfPlayers = int(input('How many players for these games: '))
    else: 
        numOfGames = int(sys.argv[1:][0])
        numOfPlayers = int(sys.argv[1:][1]) 
    assert numOfGames > 0, 'Error - Must have positive number of games.'
    assert 2 < numOfPlayers < 6, 'Error - Must have 3, 4 or 5 players.'

    players = ['Rod','Vane','Mikey','Phil','Joel','Justin','Mark','Al','Lee','James']
    
    f = open(os.path.join('FlatFiles','GameSetUp.json'), mode='w', encoding='UTF-8')
    
    i = 0
    while i < numOfGames:
        if i>0: f.write('\n')
        playerNames = random.sample(players, numOfPlayers)
        gameSetup = {}
        gameSetup['numOfPlayers'], gameSetup['playerNames'] = numOfPlayers, playerNames
        json.dump(gameSetup, f, ensure_ascii=False)
        i += 1

    f.close

if __name__ == '__main__':
    CreateGames()