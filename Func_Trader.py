"""
The Trader Function.  One of the 7 roles of the Puerto Rico game.
"""

import Func_WriteLog
from Func_Decorators import checkPriv

@checkPriv
def Trader(f, supply, player, priv, tradingHouse):
    """
    Will perform the Trader role.
    Will assess what goods a player is allowed to trade at the Trading House, 
    and from this list will choose the most valuable option for trading.
    The good will be removed from the player, and the relevant money added.

    Args:
        f             The log file object
        Supply:         The supply of game resources
        player:       The active player object
        priv:         Boolean, set to True is player has the priviledge
        tradingHouse: The Trading House object
    """
    # Build shortlist as the intersection between goods the player owns, and goods that are allowed to trade.
    playerGoods, allowedGoods = set(), set()
    for good in player.ownedGoods:
        if player.ownedGoods[good] > 0: playerGoods.add(good)
    allowedGoods = tradingHouse.allowedGoods
    goodShortlist = playerGoods.intersection(allowedGoods)

    if len(goodShortlist) > 0:
        # Choose the good, opting for the highest value first:
        if 'coffee' in goodShortlist:
            chosenGood = 'coffee'
        elif 'tobacco' in goodShortlist:
            chosenGood = 'tobacco'
        elif 'sugar' in goodShortlist:
            chosenGood = 'sugar'
        elif 'indigo' in goodShortlist:
            chosenGood = 'indigo'
        elif 'corn' in goodShortlist:
            chosenGood = 'corn'
        # Place the good on the Trading House, remove it from the player's ownedGoods and player takes the money
        moneyEarned = tradingHouse.tradeGood(chosenGood)
        moneyEarned += 1 if priv == True else 0
        player.adjustGood(chosenGood, -1)
        player.adjustMoney(moneyEarned)
        Func_WriteLog.logTrader(f, player.number, chosenGood, moneyEarned, priv)
        
    else:
        Func_WriteLog.logTrader(f, player.number, 'nothing', 0, False)


